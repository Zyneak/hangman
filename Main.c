/**
Software Description: A hangman game.
You choose letters trying to guess the word before the hangman is completely drawn.
......
Developed by: Jordan Schnur 2017
CCAC North Campus\ CIT-145 North \ Prof. Jeff Seamean
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>


//Built in Word List
char *words[70] = {"Awkward","Bagpipes","Banjo","Bungler","Croquet","Crypt","Dwarves","Fervid","Fishhook","Fjord","Gazebo","Gypsy","Haiku","Haphazard","Hyphen","Ivory","Jazzy","Jiffy","Jinx","Jukebox","Kayak","Kiosk","Klutz","Memento","Mystify","Numbskull","Ostracize","Oxygen","Pajama","Phlegm","Pixel","Polka","Quad","Quip","Rhythmic","Rogue","Sphinx","Squawk","Swivel","Toady","Twelfth","Unzip","Waxy","Wildebeest","Yacht","Zealous","Zigzag","Zippy","Zombie" };
char *word;
char *answer;
int numberOfWords = 0;
int lenghtOfWord;
int gamestate = 0;
char alphabet[26] = { 'A', 'B','C' ,'D' ,'E' ,'F' ,'G' ,'H' ,'I' ,'J' ,'K' ,'L' ,'M' ,'N' ,'O' ,'P' ,'Q' ,'R' ,'S' ,'T' ,'U' ,'V' ,'W' ,'X' ,'Y' ,'Z' };
int lettersGuessed = 0;


/** Reads a file and puts it into the word array
 *  @arg char* File to be read from
 *  @returns bool Ran successfully
 */
int readFile(char* fileName) {
	FILE *file;
	int lastNewline = 0;
	file = fopen(fileName, "r");
	if (file == NULL) { //Print error if file didn't load
		perror("Failed: ");
		return 0;
	}

	//Get size of file
	fseek(file, 0, SEEK_END);
	long fsize = ftell(file); 
	fseek(file, 0, SEEK_SET);

	//Allocate memory to store file
	char *string = malloc(fsize + 1);
	fread(string, fsize-1, 1, file);
	fclose(file);
	string[fsize] = 0;

	for (int i = 0; i < fsize-1; i++) {
		if (string[i] == 10) { //If current string is equal to newline
			int count = 0;
			words[numberOfWords] = calloc((i-lastNewline)+1,sizeof(char)); //Allocate memory and clear left over data
			for (int j = lastNewline; j < i; j++) {
				words[numberOfWords][count] = string[j]; //Put letter into word array
				count++;
			}
			numberOfWords++;
			lastNewline = i+1;
		}

	}
	return 1;
}


int last = 0;
/**Choose a word from our word list
 * @returns char* Randomly chosen word
 */
char* chooseWord() {
	srand(time(NULL));

	for (int i = 0; i < 100; i++) {
		int r = rand() % (numberOfWords - 1);
		if (r == -1 && last!= 0 && r != last) { continue; }
		lenghtOfWord = strlen(words[r]);
		answer = calloc(lenghtOfWord, sizeof(char));
		word = words[r];
		for (int j = 0; j < lenghtOfWord;j++) {
			answer[j] = '_'; //Setup variable so word can be printed
		}
		last = r;
		return words[r];
	}
}

/**Renders the screen
 *
*/

void renderScreen() {
	system("cls");
	printf("Let's play Hangman!\n\n\n\n");
	
	int width = 120;
	int drawingWidth = 7;
	int hastags = 32;
	int hashtagStart = (width - hastags) / 2;
	int currentOffset = 0;
	printf("|------   ");
	currentOffset = 10;
	for (int i = currentOffset; i < (width - hastags)/2;i++) {
		printf(" ");
	}
	for (int i = 0; i < hastags; i++) {
		printf("#");
	}
	if (gamestate >= 1) {
		printf("\n|     |");
	}
	else {
		printf("\n|      ");
	}
	if (gamestate >= 2) {
		printf("\n|     0");
	}
	else {
		printf("\n|      ");
	}

	for (int i = 6; i < (width - lenghtOfWord*2) / 2; i++) {
		printf(" ");
	}

	for (int i = 0; i < lenghtOfWord; i++) {
		printf("%c ", answer[i]);
	}

	if (gamestate >= 3) {
		printf("\n|    /|\\\n");
	}
	else {
		printf("\n|       \n");
	}

	if (gamestate >= 4) {
		printf("|    / \\");
	}
	else {
		printf("|       ");
	}
	for (int i = 8; i < (width - hastags) / 2; i++) {
		printf(" ");
	}
	for (int i = 0; i < hastags; i++) {
		printf("#");
	}
	printf("\n|\n|");

	for (int i = 1; i < hashtagStart+1; i++) {
		printf(" ");
	}
	for (int i = 0; i < 26; i++) {
		if (i == 15) {
			printf("\n|___   ___");
			for (int j = 13; j < hashtagStart + 8; j++) {
				printf(" ");
			}
		}
		printf("%c ",alphabet[i]);
	}

	
}

/** Check letter against word
 *
 */
int checkLetter(char l) {
	int found = 0; //Number of letters that matched
	for (int i = 0; i < lenghtOfWord; i++) {
		if (tolower(l) == tolower(word[i])) {
			answer[i] = toupper(l);
			found++;
		}
	}
	if (found >= 1) {
		return found;
	}
	return 0;
}


int main(int argc, char **argv) {
	
	//If an argument was passed, see if it's a file. If not load built in words 
	if (argc == 2) {
		printf("Using File. Loading...\n");
		if (readFile(argv[1]) == 1) {
			printf("\nOpened file and using word list.\n\n");
		}
		else {
			printf("\nFailed to open file. Using built in word list.\n\n ");
		}
		system("pause");
		
	}
	else {
		numberOfWords = 49;
	}

	//Chose first word
	chooseWord();
	//Message to be shown to player
	char *message = "";

	while (1 == 1) { //Game loop. Runs infinitly 

		//Did player lose or win?
		if (gamestate == 4 || lettersGuessed == lenghtOfWord) {
			renderScreen();
			printf("\n\n");
			if (gamestate == 4) {
				printf("You lost!");
			}
			else {
				printf("You won!");
			}

			printf("\nPlay again(y/n): ");
			char playAgain;
			scanf(" %c", &playAgain);
			if (tolower(playAgain) == 'y') { //Reset for new game
				chooseWord();
				gamestate = 0;
				lettersGuessed = 0;
				message = "";
				for (int i = 65; i < 65 + 26; i++) {
					alphabet[i-65] = (char)i;
				}
				continue;
			}
			else { //End game loop
				break;
			}
		}

		renderScreen();
		printf("\n\n%s", message);
		printf("\nChoose Letter: ");
		char input;
		scanf(" %c", &input);

		if(islower(input) || isupper(input)) { //Is input allowed?
			input = toupper(input);
		}
		else {
			message = "Invalid Input";
			continue;
		}
		if (alphabet[input - 65] != '*') { //Have they used this letter before?
			alphabet[input - 65] = '*';
			int numberRight = checkLetter(input);
			if (numberRight >= 1) { //Does the letter match?
				message = "You got a letter correct!";
				lettersGuessed += numberRight;
			}
			else {
				message = "You got a letter incorrect!";
				gamestate++;
			}
		}
		else {
			message = "You already used this letter. ";
		}
	}


}